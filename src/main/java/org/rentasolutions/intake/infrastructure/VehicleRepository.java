package org.rentasolutions.intake.infrastructure;

import org.rentasolutions.intake.domain.Vehicl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicl, Long> {

}
