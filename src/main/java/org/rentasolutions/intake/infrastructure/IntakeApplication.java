package org.rentasolutions.intake.infrastructure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EntityScan("org.rentasolutions.intake.domain")
public class IntakeApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntakeApplication.class, args);
	}

}
